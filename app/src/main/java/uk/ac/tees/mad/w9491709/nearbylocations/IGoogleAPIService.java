package uk.ac.tees.mad.w9491709.nearbylocations;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface IGoogleAPIService {
    @GET
    Call<MyPlace> getNearByPlaces(@Url String url);

}
